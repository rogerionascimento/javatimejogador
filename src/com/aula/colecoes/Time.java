package com.aula.colecoes;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Time {
	private String nome;
	private Map<String,Jogador> jogadores = new HashMap<String,Jogador>();
	
	
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public boolean contratar(Jogador jogador) {
		jogadores.put(jogador.getNome(), jogador);
		return true;		
	}
	public String getValorFolha() {
		
	
		Double folhaSalarial = 0.0;
		for (String key : jogadores.keySet()) {
			Jogador jog = jogadores.get(key);
			folhaSalarial += jog.getSalario();
		}
		
		return folhaSalarial.toString();
	}
	public Object getJogadores() {
	
		return jogadores;
	}
	
	
}
