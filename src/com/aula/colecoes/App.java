package com.aula.colecoes;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Map;
import java.util.Scanner;


public class App {

	private static final Comparator<? super Jogador> Jogador = null;

	public static void main(String[] args) {

		int opc;

		Scanner teclado = new Scanner(System.in);

		System.out.println("Informe o nome do time: ");
		String nome = teclado.nextLine();

		Time time = new Time();				
		time.setNome(nome);
		
		System.out.println("");
		do {
			System.out.println("");
			System.out.println("1 - Contratar um Jogador");
			System.out.println("2 - Valor total da Folha Salarial");
			System.out.println("3 - Listar todos os Jogadores");
			System.out.println("4 - Sair do programa");

			opc = Integer.valueOf(teclado.nextLine());
			System.out.println("");

			
			switch (opc) {
			case 1:
				Jogador jogador = new Jogador();

				System.out.println("Informe o nome do Jogador: ");
				jogador.setNome(teclado.nextLine());

				System.out.println("Informe o sal�rio do Jogador: ");
				jogador.setSalario(Double.valueOf(teclado.nextLine()));

				if (!time.contratar(jogador))
					System.out.println("Jogador j� pertence ao time!");
				break;

			case 2:
				System.out.println("A folha salarial do " + time.getNome() + " � de R$ " + time.getValorFolha());
				break;

			case 3:
				
				System.out.println("Lista de Jogadores: ");		
				Map<String, Jogador> jogadoresObtidos = (Map<String, Jogador>) time.getJogadores();
				ArrayList<Jogador> listaJogadores = new ArrayList<Jogador>(jogadoresObtidos.values());
				
				listaJogadores.sort(new Comparator<Jogador>() {
					@Override
					public int compare(Jogador jog1, Jogador jog2) {
						return (jog1.getSalario() < jog2.getSalario()) ? 1 : -1;						
					}		
				});
				
				for (Jogador jog : listaJogadores) {
					System.out.println(jog.getNome() + " - R$ " + jog.getSalario());
				}
				
				break;

			case 4:
				break;
			default:
				System.out.println("Op��o inv�lida!");
			}

		} while (opc != 4);

		teclado.close();
	}	
}
